<?php

namespace App\Controller;

use App\Entity\Usuario;
use App\Form\ArticuloFormType;
use App\Form\UsuarioFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController{


     /**
     * @Route("/insert", name="crear_usuarios")
     */

      public function  demoinsert(EntityManagerInterface $em){
         $usuario = new Usuario;
         $usuario->setUsername('Jota');
         $usuario->setPassword('lssklmks');
         $usuario->setEmail('Jota@gmail.com');
         $usuario->setComentarios('Esto es una descripción');
         
         //guardar en BBDD
         $em->persist($usuario);
         $em->flush();

         return new Response("Usuario guardado con éxito");
     } 
     /**
     * @Route("/lista", name="listar_usuarios")
     */

      public function listarUsuarios(EntityManagerInterface $em){
        $repositorio = $em->getRepository(Usuario::class);
        $usuarios = $repositorio->findAll();

        return $this->render('usuarios/list.html.twig',
        [
            'users'=> $usuarios
        ]
        );

     } 

     /**
     * @Route("/usuarios/nuevo", name="nuevo_usuario")
     */

    public function nuevoUsuario(Request $request, EntityManagerInterface $em){


        $form = $this->createForm(UsuarioFormType::class);

        $form -> handleRequest($request);

        if($form->isSubmitted() && $form ->isValid()){
/*             dd($form->getData());
 */
            $usuario = $form->getData();

            $em->persist($usuario);
            $em->flush();
            

            return $this->redirectToRoute('listar_usuarios');
        }

        return $this->render(
            'usuarios/nuevo.html.twig',
            [
                'formulario' => $form->createView()
            ]
        );
    } 

    /**
     * @Route("/usuarios/editar/{id}", name="editar_usuario")
     */

    public function editarUsuario(Usuario $usuario, Request $request, EntityManagerInterface $em){


        $form = $this->createForm(UsuarioFormType::class, $usuario);

        $form -> handleRequest($request);

        if($form->isSubmitted() && $form ->isValid()){
/*             dd($form->getData());
 */
            $usuario = $form->getData();

            $em->persist($usuario);
            $em->flush();
            

            return $this->redirectToRoute('listar_usuarios');
        }

        return $this->render(
            'usuarios/editar.html.twig',
            [
                'formulario' => $form->createView()
            ]
        );
    } 
    /**
     * @Route("/usuarios/eliminar/{id}", name="eliminar_usuario")
     */

    public function eliminarUsuario(Usuario $usuario, EntityManagerInterface $em){

        $em->getRepository(Usuario::class);
        
        $em->remove($usuario);
        $em->flush();
        
        return $this->redirectToRoute('listar_usuarios');
    } 


     
     
}