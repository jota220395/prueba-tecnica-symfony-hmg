<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* maleteo.html.twig */
class __TwigTemplate_54516666145e084af0c1998023917638115e2eb018d5a0c3a6468ea79f70251a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "maleteo.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">

<head>
  <meta charset=\"UTF-8\">
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
  <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
  <title>Document</title>
  <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css\">
  <link href=\"https://fonts.googleapis.com/css?family=Catamaran:400,500&display=swap\" rel=\"stylesheet\">
  <link rel=\"stylesheet\" href=\"http://localhost:8000/css/assets/styles/styles.css\">
  
</head>

<body>
  <div class=\"wrapper\">
      <div class=\"container-fluid\">
        <header class=\"header\">
            <img src=\"img/logo.svg\" alt=\"Maleteo\">
        </header>
      </div>
  </div>
  <main>
    <div class=\"jumbo\">
      <div class=\"wrapper\">
        <div class=\"container-fluid\">
          <div class=\"row\">
            <div class=\"col-md-6\">
              <div class=\"jumbo__ww-title\">
                <div class=\"jumbo__w-title\">
                  <h1 class=\"jumbo__title heading1\">
                    Gana dinero guardando equipaje a viajeros como tu
                  </h1>
                  <ul class=\"jumbo__app-btns row\">
                    <li>
                      <a href=\"#\">
                        <img src=\"img/app-store.svg\"
                             alt=\"\"
                             class=\"jumbo__app-btn\">
                      </a>
                    </li>
                    <li>
                      <a href=\"\">
                        <img src=\"img/google-play.svg\"
                             alt=\"\"
                             class=\"jumbo__app-btn\">
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class=\"wrapper\">
      <div class=\"container-fluid\">
        <section class=\"howworks\">
          <h2 class=\"howworks__title heading2\">
            ¿Cómo funciona?
          </h2>
          <ol class=\"howworks__steps row\">
            <li class=\"col-md-4\">
              <div class=\"howworks__step\">
                <div class=\"howworks__step-w-bola\">
                  <div class=\"howworks__step-bola\">
                    <span class=\"heading3\">1</span>
                  </div>
                </div>
                <div class=\"howworks__step-w-txt\">
                  <p class=\"howworks__step-title heading3\">
                    Date de alta
                  </p>
                  <p class=\"howworks__step-txt\">
                    Una olla de algo más vaca que carnero, salpicón las más noches, duelos y quebrantos los sábados.
                  </p>
                </div>
              </div>
            </li>
            <li class=\"col-md-4\">
              <div class=\"howworks__step\">
                <div class=\"howworks__step-w-bola\">
                  <div class=\"howworks__step-bola\">
                    <span class=\"heading3\">2</span>
                  </div>
                </div>
                <div class=\"howworks__step-w-txt\">
                  <p class=\"howworks__step-title heading3\">
                    Publica tus espacios, horarios y tarifas</p>
                  <p class=\"howworks__step-txt\">
                    No ha mucho tiempo que vivía un hidalgo de los de lanza en astillero.
                  </p>
                </div>
              </div>
            </li>
            <li class=\"col-md-4\">
              <div class=\"howworks__step\">
                <div class=\"howworks__step-w-bola\">
                  <div class=\"howworks__step-bola\">
                    <span class=\"heading3\">3</span>
                  </div>
                </div>
                <div class=\"howworks__step-w-txt\">
                  <p class=\"howworks__step-title heading3\">
                    Recibe viajeros y gana dinero
                  </p>
                  <p class=\"howworks__step-txt\">
                    En un lugar de la Mancha, de cuyo nombre no quiero acordarme, no ha mucho tiempo.
                  </p>
                </div>
              </div>
            </li>
          </ol>
        </section>

        <section class=\"app-form\">
          <div class=\"row\">
            <div class=\"col-md-5\">
              <div class=\"app-form__w-app-img\">
                <img src=\"img/iPhoneX.png\"
                     srcset=\"img/iPhoneX@2x.png 2x, img/iPhoneX.png 1x\"
                     alt=\"\"
                     class=\"app-form__app-img\">
              </div>
            </div>
            <div class=\"col-md-5 col-md-offset-1\">
              <div class=\"the-form\">
                <form method=\"POST\" action=\"\">
                  <p class=\"the-form__title heading3\">
                    Solicita una demo
                  </p>
                  
                  <div class=\"the-form__group\">
                    ";
        // line 135
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["articuloForm"]) || array_key_exists("articuloForm", $context) ? $context["articuloForm"] : (function () { throw new RuntimeError('Variable "articuloForm" does not exist.', 135, $this->source); })()), 'form_start');
        echo "
                    <label for=\"name\">
                      Nombre 
                    </label>
                    ";
        // line 139
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["articuloForm"]) || array_key_exists("articuloForm", $context) ? $context["articuloForm"] : (function () { throw new RuntimeError('Variable "articuloForm" does not exist.', 139, $this->source); })()), 'widget');
        echo "
                  </div>

                  <div class=\"the-form__group\">
                    <label for=\"name\">
                      Email
                    </label>
                    <input type=\"email\"
                          id=\"name\"
                          class=\"the-form__input\"
                          placeholder=\"juanjo134@gmail.com\">
                  </div>

                  <div class=\"the-form__group\">
                    <label for=\"horario\">
                      Horario preferido
                    </label>
                    <input type=\"text\"
                          id=\"horario\"
                          class=\"the-form__input\"
                          placeholder=\"de 18:00 a 22:00\">
                  </div>

                  <div class=\"the-form__group\">
                    <label for=\"city\">
                      Ciudad
                    </label>
                    <select name=\"\"
                            id=\"city\"
                            class=\"the-form__input the-form__select\">
                      <option value=\"\">Madrid</option>
                      <option value=\"\">Barcelona</option>
                      <option value=\"\">Valencia</option>
                    </select>
                  </div>

                  <div class=\"the-form__group the-form__group_checkbox\">
                    <input type=\"checkbox\"
                          id=\"privacity\">
                    <label for=\"privacity\">
                      <a href=\"#\">He leído y acepto la política de privacidad</a>
                    </label>
                  </div>

                  <input type=\"button\"
                        class=\"the-form__btn btn btn-lg btn-primary\"
                        value=\"Enviar\">
                        ";
        // line 186
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["articuloForm"]) || array_key_exists("articuloForm", $context) ? $context["articuloForm"] : (function () { throw new RuntimeError('Variable "articuloForm" does not exist.', 186, $this->source); })()), 'form_end');
        echo "
                </form>
              </div>
            </div>
          </div>
        </section>

        <section class=\"opinions\">
          <h2 class=\"opinions__title heading2\">
            Opiniones de otros guardianes
          </h2>
          <ul class=\"row\">
            <li class=\"col-md-4\">
              <div class=\"opinion\">
                <blockquote class=\"opinion__blq simple-bubble text-large\">
                  Tras el enorme éxito internacional de su primera colaboración, “Bailar”, que ganó un galardón en los Premios
                </blockquote>
                <p class=\"opinion__author text-large\">
                  Sergio Garnacho
                </p>
                <p class=\"opinion__author-address\">
                  Tetuán, Madrid
                </p>
              </div>
            </li>
            <li class=\"col-md-4\">
              <div class=\"opinion\">
                <blockquote class=\"opinion__blq simple-bubble text-large\">
                  Tras el enorme éxito internacional de su primera colaboración, “Bailar”, que ganó un galardón en los Premios
                </blockquote>
                <p class=\"opinion__author text-large\">
                  Sergio Garnacho
                </p>
                <p class=\"opinion__author-address\">
                  Tetuán, Madrid
                </p>
              </div>
            </li>
            <li class=\"col-md-4\">
              <div class=\"opinion\">
                <blockquote class=\"opinion__blq simple-bubble text-large\">
                  Tras el enorme éxito internacional de su primera colaboración, “Bailar”, que ganó un galardón en los Premios
                </blockquote>
                <p class=\"opinion__author text-large\">
                  Sergio Garnacho
                </p>
                <p class=\"opinion__author-address\">
                  Tetuán, Madrid
                </p>
              </div>
            </li>
          </ul>
        </section>
      </div>  
    </div>
<div class=\"mapa\">
  <div class=\"mapa__title col-xs-8 col-md-12\"><h2>Guardianes cerca tuyo</h2>
  </div>
  <iframe class=\"mapa__map\" src=\"https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d12147.560253557589!2d-3.66576724156495!3d40.43343317544412!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses!2ses!4v1575743718905!5m2!1ses!2ses\" width=\"80%\" height=\"300\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>
</div>
  </main>
  <footer class=\"footer\">
    <div class=\"wrapper\">
      <div class=\"container-fluid\">
        <img src=\"img/logowhite.svg\"
            alt=\"Maleteo\"
            class=\"footer__logo\">
        <p class=\"footer__claim\">
          Hecho con ❤️en Madrid
        </p>
      </div>
    </div>
  </footer>
</body>

</html>";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "maleteo.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  233 => 186,  183 => 139,  176 => 135,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"en\">

<head>
  <meta charset=\"UTF-8\">
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
  <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
  <title>Document</title>
  <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css\">
  <link href=\"https://fonts.googleapis.com/css?family=Catamaran:400,500&display=swap\" rel=\"stylesheet\">
  <link rel=\"stylesheet\" href=\"http://localhost:8000/css/assets/styles/styles.css\">
  
</head>

<body>
  <div class=\"wrapper\">
      <div class=\"container-fluid\">
        <header class=\"header\">
            <img src=\"img/logo.svg\" alt=\"Maleteo\">
        </header>
      </div>
  </div>
  <main>
    <div class=\"jumbo\">
      <div class=\"wrapper\">
        <div class=\"container-fluid\">
          <div class=\"row\">
            <div class=\"col-md-6\">
              <div class=\"jumbo__ww-title\">
                <div class=\"jumbo__w-title\">
                  <h1 class=\"jumbo__title heading1\">
                    Gana dinero guardando equipaje a viajeros como tu
                  </h1>
                  <ul class=\"jumbo__app-btns row\">
                    <li>
                      <a href=\"#\">
                        <img src=\"img/app-store.svg\"
                             alt=\"\"
                             class=\"jumbo__app-btn\">
                      </a>
                    </li>
                    <li>
                      <a href=\"\">
                        <img src=\"img/google-play.svg\"
                             alt=\"\"
                             class=\"jumbo__app-btn\">
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class=\"wrapper\">
      <div class=\"container-fluid\">
        <section class=\"howworks\">
          <h2 class=\"howworks__title heading2\">
            ¿Cómo funciona?
          </h2>
          <ol class=\"howworks__steps row\">
            <li class=\"col-md-4\">
              <div class=\"howworks__step\">
                <div class=\"howworks__step-w-bola\">
                  <div class=\"howworks__step-bola\">
                    <span class=\"heading3\">1</span>
                  </div>
                </div>
                <div class=\"howworks__step-w-txt\">
                  <p class=\"howworks__step-title heading3\">
                    Date de alta
                  </p>
                  <p class=\"howworks__step-txt\">
                    Una olla de algo más vaca que carnero, salpicón las más noches, duelos y quebrantos los sábados.
                  </p>
                </div>
              </div>
            </li>
            <li class=\"col-md-4\">
              <div class=\"howworks__step\">
                <div class=\"howworks__step-w-bola\">
                  <div class=\"howworks__step-bola\">
                    <span class=\"heading3\">2</span>
                  </div>
                </div>
                <div class=\"howworks__step-w-txt\">
                  <p class=\"howworks__step-title heading3\">
                    Publica tus espacios, horarios y tarifas</p>
                  <p class=\"howworks__step-txt\">
                    No ha mucho tiempo que vivía un hidalgo de los de lanza en astillero.
                  </p>
                </div>
              </div>
            </li>
            <li class=\"col-md-4\">
              <div class=\"howworks__step\">
                <div class=\"howworks__step-w-bola\">
                  <div class=\"howworks__step-bola\">
                    <span class=\"heading3\">3</span>
                  </div>
                </div>
                <div class=\"howworks__step-w-txt\">
                  <p class=\"howworks__step-title heading3\">
                    Recibe viajeros y gana dinero
                  </p>
                  <p class=\"howworks__step-txt\">
                    En un lugar de la Mancha, de cuyo nombre no quiero acordarme, no ha mucho tiempo.
                  </p>
                </div>
              </div>
            </li>
          </ol>
        </section>

        <section class=\"app-form\">
          <div class=\"row\">
            <div class=\"col-md-5\">
              <div class=\"app-form__w-app-img\">
                <img src=\"img/iPhoneX.png\"
                     srcset=\"img/iPhoneX@2x.png 2x, img/iPhoneX.png 1x\"
                     alt=\"\"
                     class=\"app-form__app-img\">
              </div>
            </div>
            <div class=\"col-md-5 col-md-offset-1\">
              <div class=\"the-form\">
                <form method=\"POST\" action=\"\">
                  <p class=\"the-form__title heading3\">
                    Solicita una demo
                  </p>
                  
                  <div class=\"the-form__group\">
                    {{form_start(articuloForm)}}
                    <label for=\"name\">
                      Nombre 
                    </label>
                    {{form_widget(articuloForm)}}
                  </div>

                  <div class=\"the-form__group\">
                    <label for=\"name\">
                      Email
                    </label>
                    <input type=\"email\"
                          id=\"name\"
                          class=\"the-form__input\"
                          placeholder=\"juanjo134@gmail.com\">
                  </div>

                  <div class=\"the-form__group\">
                    <label for=\"horario\">
                      Horario preferido
                    </label>
                    <input type=\"text\"
                          id=\"horario\"
                          class=\"the-form__input\"
                          placeholder=\"de 18:00 a 22:00\">
                  </div>

                  <div class=\"the-form__group\">
                    <label for=\"city\">
                      Ciudad
                    </label>
                    <select name=\"\"
                            id=\"city\"
                            class=\"the-form__input the-form__select\">
                      <option value=\"\">Madrid</option>
                      <option value=\"\">Barcelona</option>
                      <option value=\"\">Valencia</option>
                    </select>
                  </div>

                  <div class=\"the-form__group the-form__group_checkbox\">
                    <input type=\"checkbox\"
                          id=\"privacity\">
                    <label for=\"privacity\">
                      <a href=\"#\">He leído y acepto la política de privacidad</a>
                    </label>
                  </div>

                  <input type=\"button\"
                        class=\"the-form__btn btn btn-lg btn-primary\"
                        value=\"Enviar\">
                        {{form_end(articuloForm)}}
                </form>
              </div>
            </div>
          </div>
        </section>

        <section class=\"opinions\">
          <h2 class=\"opinions__title heading2\">
            Opiniones de otros guardianes
          </h2>
          <ul class=\"row\">
            <li class=\"col-md-4\">
              <div class=\"opinion\">
                <blockquote class=\"opinion__blq simple-bubble text-large\">
                  Tras el enorme éxito internacional de su primera colaboración, “Bailar”, que ganó un galardón en los Premios
                </blockquote>
                <p class=\"opinion__author text-large\">
                  Sergio Garnacho
                </p>
                <p class=\"opinion__author-address\">
                  Tetuán, Madrid
                </p>
              </div>
            </li>
            <li class=\"col-md-4\">
              <div class=\"opinion\">
                <blockquote class=\"opinion__blq simple-bubble text-large\">
                  Tras el enorme éxito internacional de su primera colaboración, “Bailar”, que ganó un galardón en los Premios
                </blockquote>
                <p class=\"opinion__author text-large\">
                  Sergio Garnacho
                </p>
                <p class=\"opinion__author-address\">
                  Tetuán, Madrid
                </p>
              </div>
            </li>
            <li class=\"col-md-4\">
              <div class=\"opinion\">
                <blockquote class=\"opinion__blq simple-bubble text-large\">
                  Tras el enorme éxito internacional de su primera colaboración, “Bailar”, que ganó un galardón en los Premios
                </blockquote>
                <p class=\"opinion__author text-large\">
                  Sergio Garnacho
                </p>
                <p class=\"opinion__author-address\">
                  Tetuán, Madrid
                </p>
              </div>
            </li>
          </ul>
        </section>
      </div>  
    </div>
<div class=\"mapa\">
  <div class=\"mapa__title col-xs-8 col-md-12\"><h2>Guardianes cerca tuyo</h2>
  </div>
  <iframe class=\"mapa__map\" src=\"https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d12147.560253557589!2d-3.66576724156495!3d40.43343317544412!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses!2ses!4v1575743718905!5m2!1ses!2ses\" width=\"80%\" height=\"300\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>
</div>
  </main>
  <footer class=\"footer\">
    <div class=\"wrapper\">
      <div class=\"container-fluid\">
        <img src=\"img/logowhite.svg\"
            alt=\"Maleteo\"
            class=\"footer__logo\">
        <p class=\"footer__claim\">
          Hecho con ❤️en Madrid
        </p>
      </div>
    </div>
  </footer>
</body>

</html>", "maleteo.html.twig", "/application/templates/maleteo.html.twig");
    }
}
