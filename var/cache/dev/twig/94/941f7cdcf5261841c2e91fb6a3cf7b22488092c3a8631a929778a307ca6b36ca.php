<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* maleteo/consultas.html.twig */
class __TwigTemplate_2ebe586bdd7b001723b822bbf60e4c4f4e634e8e436612b14ba795174107172d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "maleteo/consultas.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">

<head>
  <meta charset=\"UTF-8\">
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
  <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
  <title>Document</title>
  <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css\">
  <link href=\"https://fonts.googleapis.com/css?family=Catamaran:400,500&display=swap\" rel=\"stylesheet\">
  <link rel=\"stylesheet\" href=\"http://localhost:8000/css/assets/styles/styles.css\">
  
</head>

<body>
  <div class=\"wrapper\">
      <div class=\"container-fluid\">
        <header class=\"header\">
            <img src=\"img/logo.svg\" alt=\"Maleteo\">
        </header>
      </div>
  </div>

  <footer class=\"footer__valida\">
    <div class=\"wrapper\">
      <div class=\"container-fluid\">
        <img src=\"img/logowhite.svg\"
            alt=\"Maleteo\"
            class=\"footer__logo\">
        <p class=\"footer__claim\">
          Hecho con ❤️en Madrid
        </p>
      </div>
    </div>
  </footer>
</body>

</html>";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "maleteo/consultas.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"en\">

<head>
  <meta charset=\"UTF-8\">
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
  <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
  <title>Document</title>
  <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css\">
  <link href=\"https://fonts.googleapis.com/css?family=Catamaran:400,500&display=swap\" rel=\"stylesheet\">
  <link rel=\"stylesheet\" href=\"http://localhost:8000/css/assets/styles/styles.css\">
  
</head>

<body>
  <div class=\"wrapper\">
      <div class=\"container-fluid\">
        <header class=\"header\">
            <img src=\"img/logo.svg\" alt=\"Maleteo\">
        </header>
      </div>
  </div>

  <footer class=\"footer__valida\">
    <div class=\"wrapper\">
      <div class=\"container-fluid\">
        <img src=\"img/logowhite.svg\"
            alt=\"Maleteo\"
            class=\"footer__logo\">
        <p class=\"footer__claim\">
          Hecho con ❤️en Madrid
        </p>
      </div>
    </div>
  </footer>
</body>

</html>", "maleteo/consultas.html.twig", "/application/templates/maleteo/consultas.html.twig");
    }
}
