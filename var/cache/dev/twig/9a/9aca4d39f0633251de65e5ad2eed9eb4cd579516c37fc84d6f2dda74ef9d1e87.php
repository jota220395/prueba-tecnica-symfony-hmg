<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/home.html.twig */
class __TwigTemplate_aed667b605700a8e9935d7fbba28dbe6283b3d69e379f9b86a181cddee5d637f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/home.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">

<head>

    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <title>";
        // line 9
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
    ";
        // line 10
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 17
        echo "
</head>

<body>


    <header>
        <div class=\"encabezado\">
            <img src=\"img/hmg.png\">
            <h1>Prueba de código </h1>
            <h2>J. Javier Martínez</h2>
        </div>

    </header>

    <main>
        <div class=\"home\">
            <div class=\"home__iniciar\"><a href=\"\">Iniciar sesión</a>
                <form action=\"\" method=\"POST\">
                    <label>Username: </label>
                    <input type=\"text\" name=\"username\" />
                    <label>Password: </label>
                    <input type=\"password\" name=\"password\" />
                    <input type=\"submit\" name=\"enviar\" value=\"Iniciar sesión\" />
                </form>
            </div>
            <div class=\"home__registro\"><a href=\"";
        // line 43
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("registro");
        echo "\">Registrarse</a>
            </div>
        </div>
    </main>
    <footer>
        <div class=\"pie\">
            <p>Prueba realizado el:19/06/2020 </p>
        </div>
    </footer>
</body>

</html>";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 9
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Prueba HMG";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 10
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 11
        echo "    <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css\">
    <link href=\"https://fonts.googleapis.com/css?family=Catamaran:400,500&display=swap\" rel=\"stylesheet\">
    <link rel=\"stylesheet\" href=\"css/estilosHome.css\">
    <script src=\"https://cdn.jsdelivr.net/npm/squirrelly@7.5.0/dist/squirrelly.min.js\"></script>

    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "home/home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  125 => 11,  118 => 10,  105 => 9,  86 => 43,  58 => 17,  56 => 10,  52 => 9,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"en\">

<head>

    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <title>{% block title %}Prueba HMG{% endblock %}</title>
    {%block stylesheets %}
    <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css\">
    <link href=\"https://fonts.googleapis.com/css?family=Catamaran:400,500&display=swap\" rel=\"stylesheet\">
    <link rel=\"stylesheet\" href=\"css/estilosHome.css\">
    <script src=\"https://cdn.jsdelivr.net/npm/squirrelly@7.5.0/dist/squirrelly.min.js\"></script>

    {% endblock %}

</head>

<body>


    <header>
        <div class=\"encabezado\">
            <img src=\"img/hmg.png\">
            <h1>Prueba de código </h1>
            <h2>J. Javier Martínez</h2>
        </div>

    </header>

    <main>
        <div class=\"home\">
            <div class=\"home__iniciar\"><a href=\"\">Iniciar sesión</a>
                <form action=\"\" method=\"POST\">
                    <label>Username: </label>
                    <input type=\"text\" name=\"username\" />
                    <label>Password: </label>
                    <input type=\"password\" name=\"password\" />
                    <input type=\"submit\" name=\"enviar\" value=\"Iniciar sesión\" />
                </form>
            </div>
            <div class=\"home__registro\"><a href=\"{{path('registro')}}\">Registrarse</a>
            </div>
        </div>
    </main>
    <footer>
        <div class=\"pie\">
            <p>Prueba realizado el:19/06/2020 </p>
        </div>
    </footer>
</body>

</html>", "home/home.html.twig", "/application/templates/home/home.html.twig");
    }
}
