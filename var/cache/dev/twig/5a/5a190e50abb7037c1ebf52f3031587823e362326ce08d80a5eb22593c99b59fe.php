<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* usuarios/list.html.twig */
class __TwigTemplate_c296cfd4c9185d905e06c68945c5ea950b8d5a651f90e194ee541b3e7f26353b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "usuarios/list.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "usuarios/list.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<table class=\"tabla_usuarios\">
    <tr class=\"tr0\">
        <td colspan=\"5\">Lista de usuarios
        </td>
    </tr>

    <tr class=\"tr1\">
        <td>Username
        </td>
        <td>Password
        </td>
        <td>Email
        </td>
        <td>Comentarios
        </td>
        <td>Acciones
        </td>
    </tr>
    ";
        // line 22
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new RuntimeError('Variable "users" does not exist.', 22, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["usuario"]) {
            // line 23
            echo "    <tr>
        <td>";
            // line 24
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["usuario"], "username", [], "any", false, false, false, 24), "html", null, true);
            echo " </td>
        <td>";
            // line 25
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["usuario"], "password", [], "any", false, false, false, 25), "html", null, true);
            echo "</td>
        <td>";
            // line 26
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["usuario"], "email", [], "any", false, false, false, 26), "html", null, true);
            echo "</td>
        <td>";
            // line 27
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["usuario"], "comentarios", [], "any", false, false, false, 27), "html", null, true);
            echo "</td>
        <td>
        <button class=\"btn\"> <a href=\" ";
            // line 29
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("editar_usuario", ["id" => twig_get_attribute($this->env, $this->source, $context["usuario"], "id", [], "any", false, false, false, 29)]), "html", null, true);
            echo " \">Editar </a></button>
        <button class=\"btn\"><a href=\" ";
            // line 30
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("eliminar_usuario", ["id" => twig_get_attribute($this->env, $this->source, $context["usuario"], "id", [], "any", false, false, false, 30)]), "html", null, true);
            echo " \">Eliminar</a></button>
        </td>
    </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['usuario'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 34
        echo "    <tr class=\"tr3\">
    <td colspan=\"5\">
    <button><a href=\" ";
        // line 36
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nuevo_usuario");
        echo "\"> Crear usuario </a>
    </button>
    <button><a href=\" ";
        // line 38
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("home");
        echo "\"> Salir => </a>
    </button>
    </td>
    </tr>

</table>


";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "usuarios/list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  126 => 38,  121 => 36,  117 => 34,  107 => 30,  103 => 29,  98 => 27,  94 => 26,  90 => 25,  86 => 24,  83 => 23,  79 => 22,  59 => 4,  52 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}
<table class=\"tabla_usuarios\">
    <tr class=\"tr0\">
        <td colspan=\"5\">Lista de usuarios
        </td>
    </tr>

    <tr class=\"tr1\">
        <td>Username
        </td>
        <td>Password
        </td>
        <td>Email
        </td>
        <td>Comentarios
        </td>
        <td>Acciones
        </td>
    </tr>
    {% for usuario in users %}
    <tr>
        <td>{{usuario.username}} </td>
        <td>{{usuario.password}}</td>
        <td>{{usuario.email}}</td>
        <td>{{usuario.comentarios}}</td>
        <td>
        <button class=\"btn\"> <a href=\" {{path('editar_usuario', {id:usuario.id}) }} \">Editar </a></button>
        <button class=\"btn\"><a href=\" {{path('eliminar_usuario', {id:usuario.id}) }} \">Eliminar</a></button>
        </td>
    </tr>
    {% endfor %}
    <tr class=\"tr3\">
    <td colspan=\"5\">
    <button><a href=\" {{path('nuevo_usuario') }}\"> Crear usuario </a>
    </button>
    <button><a href=\" {{path('home') }}\"> Salir => </a>
    </button>
    </td>
    </tr>

</table>


{% endblock %}", "usuarios/list.html.twig", "/application/templates/usuarios/list.html.twig");
    }
}
