<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* maleteo/maleteo.html.twig */
class __TwigTemplate_87fdb261a384346d9d4a7e271b07342f298000da4084a6fe686aa517b50bea58 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "maleteo/maleteo.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">

<head>

  <meta charset=\"UTF-8\">
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
  <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
  <title>";
        // line 9
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
  ";
        // line 10
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 19
        echo "  
</head>

<body>
  
  <div class=\"wrapper\">
      <div class=\"container-fluid\">
        <header class=\"header\">
            
              <img src=\"img/logo.svg\" alt=\"Maleteo\">
            </a>
        </header>
      </div>
  </div>
  <main>
    <div class=\"jumbo\">
      <div class=\"wrapper\">
        <div class=\"container-fluid\">
          <div class=\"row\">
            <div class=\"col-md-6\">
              <div class=\"jumbo__ww-title\">
                <div class=\"jumbo__w-title\">
                  <h1 class=\"jumbo__title heading1\">
                    Gana dinero guardando equipaje a viajeros como tu
                  </h1>
                  <ul class=\"jumbo__app-btns row\">
                    <li>
                      <a href=\"#\">
                        <img src=\"img/app-store.svg\"
                             alt=\"\"
                             class=\"jumbo__app-btn\">
                      </a>
                    </li>
                    <li>
                      <a href=\"\">
                        <img src=\"img/google-play.svg\"
                             alt=\"\"
                             class=\"jumbo__app-btn\">
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class=\"wrapper\">
      <div class=\"container-fluid\">
        <section class=\"howworks\">
          <h2 class=\"howworks__title heading2\">
            ¿Cómo funciona?
          </h2>
          <ol class=\"howworks__steps row\">
            <li class=\"col-md-4\">
              <div class=\"howworks__step\">
                <div class=\"howworks__step-w-bola\">
                  <div class=\"howworks__step-bola\">
                    <span class=\"heading3\">1</span>
                  </div>
                </div>
                <div class=\"howworks__step-w-txt\">
                  <p class=\"howworks__step-title heading3\">
                    Date de alta
                  </p>
                  <p class=\"howworks__step-txt\">
                    Una olla de algo más vaca que carnero, salpicón las más noches, duelos y quebrantos los sábados.
                  </p>
                </div>
              </div>
            </li>
            <li class=\"col-md-4\">
              <div class=\"howworks__step\">
                <div class=\"howworks__step-w-bola\">
                  <div class=\"howworks__step-bola\">
                    <span class=\"heading3\">2</span>
                  </div>
                </div>
                <div class=\"howworks__step-w-txt\">
                  <p class=\"howworks__step-title heading3\">
                    Publica tus espacios, horarios y tarifas</p>
                  <p class=\"howworks__step-txt\">
                    No ha mucho tiempo que vivía un hidalgo de los de lanza en astillero.
                  </p>
                </div>
              </div>
            </li>
            <li class=\"col-md-4\">
              <div class=\"howworks__step\">
                <div class=\"howworks__step-w-bola\">
                  <div class=\"howworks__step-bola\">
                    <span class=\"heading3\">3</span>
                  </div>
                </div>
                <div class=\"howworks__step-w-txt\">
                  <p class=\"howworks__step-title heading3\">
                    Recibe viajeros y gana dinero
                  </p>
                  <p class=\"howworks__step-txt\">
                    En un lugar de la Mancha, de cuyo nombre no quiero acordarme, no ha mucho tiempo.
                  </p>
                </div>
              </div>
            </li>
          </ol>
        </section>

        <section class=\"app-form\">
          <div class=\"row\">
            <div class=\"col-md-5\">
              <div class=\"app-form__w-app-img\">
                <img src=\"img/iPhoneX.png\"
                     srcset=\"img/iPhoneX@2x.png 2x, img/iPhoneX.png 1x\"
                     alt=\"\"
                     class=\"app-form__app-img\">
              </div>
            </div>
            <div class=\"col-md-5 col-md-offset-1\">
              <div  id=\"formprincipal\" class=\"the-form\">
              <div id=\"exito\">
                  <form method=\"post\" action=\"/maleteo\" id=\"formulario\">  
                  <p class=\"the-form__title heading3\">
                   
                    Solicita una demo
                   ";
        // line 144
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["DemoForm"]) || array_key_exists("DemoForm", $context) ? $context["DemoForm"] : (function () { throw new RuntimeError('Variable "DemoForm" does not exist.', 144, $this->source); })()), 'form_start');
        echo "
                    <div class=\"formulario__form\" id=\"formulario\" >
                    <p class=\"formulario__demo\">
                    ";
        // line 147
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["DemoForm"]) || array_key_exists("DemoForm", $context) ? $context["DemoForm"] : (function () { throw new RuntimeError('Variable "DemoForm" does not exist.', 147, $this->source); })()), "nombre", [], "any", false, false, false, 147), 'row', ["label" => "Nombre", "attr" => ["class" => "the-form__input", "placeholder" => "Nombre"]]);
        echo "
                    ";
        // line 148
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["DemoForm"]) || array_key_exists("DemoForm", $context) ? $context["DemoForm"] : (function () { throw new RuntimeError('Variable "DemoForm" does not exist.', 148, $this->source); })()), "email", [], "any", false, false, false, 148), 'row', ["label" => "email", "attr" => ["class" => "the-form__input", "placeholder" => "Email"]]);
        echo "
                    ";
        // line 149
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["DemoForm"]) || array_key_exists("DemoForm", $context) ? $context["DemoForm"] : (function () { throw new RuntimeError('Variable "DemoForm" does not exist.', 149, $this->source); })()), "horario", [], "any", false, false, false, 149), 'row', ["label" => "horario", "attr" => ["class" => "the-form__input", "placeholder" => "Mañana"]]);
        echo "
                    ";
        // line 150
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["DemoForm"]) || array_key_exists("DemoForm", $context) ? $context["DemoForm"] : (function () { throw new RuntimeError('Variable "DemoForm" does not exist.', 150, $this->source); })()), "ciudad", [], "any", false, false, false, 150), 'row', ["label" => "ciudad", "attr" => ["class" => "the-form__input the-form__select", "placeholder" => "Tres Cantos"]]);
        echo "
                    ";
        // line 151
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["DemoForm"]) || array_key_exists("DemoForm", $context) ? $context["DemoForm"] : (function () { throw new RuntimeError('Variable "DemoForm" does not exist.', 151, $this->source); })()), "Politicadeprivacidad", [], "any", false, false, false, 151), 'row', ["attr" => ["class" => "the-form__group"]]);
        echo "
                    ";
        // line 152
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["DemoForm"]) || array_key_exists("DemoForm", $context) ? $context["DemoForm"] : (function () { throw new RuntimeError('Variable "DemoForm" does not exist.', 152, $this->source); })()), "Enviar", [], "any", false, false, false, 152), 'row', ["attr" => ["class" => "the-form__btn btn btn-lg btn-primary"]]);
        echo "
                    </div>
                    ";
        // line 154
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["DemoForm"]) || array_key_exists("DemoForm", $context) ? $context["DemoForm"] : (function () { throw new RuntimeError('Variable "DemoForm" does not exist.', 154, $this->source); })()), 'form_end');
        echo "
                    </div>
                    </form>
                  </p>
                  
                  
              </div>
            </div>
          </div>
        </section>

        <section class=\"opinions\">
          <h2 class=\"opinions__title heading2\">
            Opiniones de otros guardianes
          </h2>
          <ul class=\"row\">
            <li class=\"col-md-4\">
              <div class=\"opinion\">
                <blockquote class=\"opinion__blq simple-bubble text-large\">
                  Tras el enorme éxito internacional de su primera colaboración, “Bailar”, que ganó un galardón en los Premios
                </blockquote>
                <p class=\"opinion__author text-large\">
                  Sergio Garnacho
                </p>
                <p class=\"opinion__author-address\">
                  Tetuán, Madrid
                </p>
              </div>
            </li>
            <li class=\"col-md-4\">
              <div class=\"opinion\">
                <blockquote class=\"opinion__blq simple-bubble text-large\">
                  Tras el enorme éxito internacional de su primera colaboración, “Bailar”, que ganó un galardón en los Premios
                </blockquote>
                <p class=\"opinion__author text-large\">
                  Sergio Sánchez
                </p>
                <p class=\"opinion__author-address\">
                  Barrio del Pilar, Madrid
                </p>
              </div>
            </li>
            <li class=\"col-md-4\">
              <div class=\"opinion\">
                <blockquote class=\"opinion__blq simple-bubble text-large\">
                  Tras el enorme éxito internacional de su primera colaboración, “Bailar”, que ganó un galardón en los Premios
                </blockquote>
                <p class=\"opinion__author text-large\">
                  Sergio Garnacho
                </p>
                <p class=\"opinion__author-address\">
                  Tetuán, Madrid
                </p>
              </div>
            </li>
          </ul>
        </section>
      </div>  
    </div>
 <div class=\"mapa \">
    <div class=\"mapa__title\"><h2>Guardianes cerca tuyo</h2>
    </div>
    <div class=\"row\">
        <div class =\"col-xs-4 mapa__valoracion\" id=\"valoracion\">
          
        </div>

        <div class=\" col-xs-8 mapa__map\"><iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d12147.560253557589!2d-3.66576724156495!3d40.43343317544412!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses!2ses!4v1575743718905!5m2!1ses!2ses\" width=\"80%\" height=\"300\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>
        </div>
    </div>
  </div>
</main>
  <footer class=\"footer\">
    <div class=\"wrapper\">
      <div class=\"container-fluid\">
        <img src=\"img/logowhite.svg\"
            alt=\"Maleteo\"
            class=\"footer__logo\">
        <p class=\"footer__claim\">
          Hecho con ❤️en Madrid
        </p>
      </div>
    </div>
  </footer>
</body>

</html>";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 9
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Maleteo";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 10
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 11
        echo "  <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css\">
  <link href=\"https://fonts.googleapis.com/css?family=Catamaran:400,500&display=swap\" rel=\"stylesheet\">
  <link rel=\"stylesheet\" href=\"http://localhost:8000/css/assets/styles/styles.css\">
  <script type=\"text/javascript\" src=\"valoracion.js\"> </script>
  <script src=\"https://cdn.jsdelivr.net/npm/squirrelly@7.5.0/dist/squirrelly.min.js\"></script>
  <script type=\"text/javascript\" src=\"formulario.js\"> </script>
 
  ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "maleteo/maleteo.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  330 => 11,  323 => 10,  310 => 9,  216 => 154,  211 => 152,  207 => 151,  203 => 150,  199 => 149,  195 => 148,  191 => 147,  185 => 144,  58 => 19,  56 => 10,  52 => 9,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"en\">

<head>

  <meta charset=\"UTF-8\">
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
  <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
  <title>{% block title %}Maleteo{% endblock %}</title>
  {%block stylesheets %}
  <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css\">
  <link href=\"https://fonts.googleapis.com/css?family=Catamaran:400,500&display=swap\" rel=\"stylesheet\">
  <link rel=\"stylesheet\" href=\"http://localhost:8000/css/assets/styles/styles.css\">
  <script type=\"text/javascript\" src=\"valoracion.js\"> </script>
  <script src=\"https://cdn.jsdelivr.net/npm/squirrelly@7.5.0/dist/squirrelly.min.js\"></script>
  <script type=\"text/javascript\" src=\"formulario.js\"> </script>
 
  {% endblock %}
  
</head>

<body>
  
  <div class=\"wrapper\">
      <div class=\"container-fluid\">
        <header class=\"header\">
            
              <img src=\"img/logo.svg\" alt=\"Maleteo\">
            </a>
        </header>
      </div>
  </div>
  <main>
    <div class=\"jumbo\">
      <div class=\"wrapper\">
        <div class=\"container-fluid\">
          <div class=\"row\">
            <div class=\"col-md-6\">
              <div class=\"jumbo__ww-title\">
                <div class=\"jumbo__w-title\">
                  <h1 class=\"jumbo__title heading1\">
                    Gana dinero guardando equipaje a viajeros como tu
                  </h1>
                  <ul class=\"jumbo__app-btns row\">
                    <li>
                      <a href=\"#\">
                        <img src=\"img/app-store.svg\"
                             alt=\"\"
                             class=\"jumbo__app-btn\">
                      </a>
                    </li>
                    <li>
                      <a href=\"\">
                        <img src=\"img/google-play.svg\"
                             alt=\"\"
                             class=\"jumbo__app-btn\">
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class=\"wrapper\">
      <div class=\"container-fluid\">
        <section class=\"howworks\">
          <h2 class=\"howworks__title heading2\">
            ¿Cómo funciona?
          </h2>
          <ol class=\"howworks__steps row\">
            <li class=\"col-md-4\">
              <div class=\"howworks__step\">
                <div class=\"howworks__step-w-bola\">
                  <div class=\"howworks__step-bola\">
                    <span class=\"heading3\">1</span>
                  </div>
                </div>
                <div class=\"howworks__step-w-txt\">
                  <p class=\"howworks__step-title heading3\">
                    Date de alta
                  </p>
                  <p class=\"howworks__step-txt\">
                    Una olla de algo más vaca que carnero, salpicón las más noches, duelos y quebrantos los sábados.
                  </p>
                </div>
              </div>
            </li>
            <li class=\"col-md-4\">
              <div class=\"howworks__step\">
                <div class=\"howworks__step-w-bola\">
                  <div class=\"howworks__step-bola\">
                    <span class=\"heading3\">2</span>
                  </div>
                </div>
                <div class=\"howworks__step-w-txt\">
                  <p class=\"howworks__step-title heading3\">
                    Publica tus espacios, horarios y tarifas</p>
                  <p class=\"howworks__step-txt\">
                    No ha mucho tiempo que vivía un hidalgo de los de lanza en astillero.
                  </p>
                </div>
              </div>
            </li>
            <li class=\"col-md-4\">
              <div class=\"howworks__step\">
                <div class=\"howworks__step-w-bola\">
                  <div class=\"howworks__step-bola\">
                    <span class=\"heading3\">3</span>
                  </div>
                </div>
                <div class=\"howworks__step-w-txt\">
                  <p class=\"howworks__step-title heading3\">
                    Recibe viajeros y gana dinero
                  </p>
                  <p class=\"howworks__step-txt\">
                    En un lugar de la Mancha, de cuyo nombre no quiero acordarme, no ha mucho tiempo.
                  </p>
                </div>
              </div>
            </li>
          </ol>
        </section>

        <section class=\"app-form\">
          <div class=\"row\">
            <div class=\"col-md-5\">
              <div class=\"app-form__w-app-img\">
                <img src=\"img/iPhoneX.png\"
                     srcset=\"img/iPhoneX@2x.png 2x, img/iPhoneX.png 1x\"
                     alt=\"\"
                     class=\"app-form__app-img\">
              </div>
            </div>
            <div class=\"col-md-5 col-md-offset-1\">
              <div  id=\"formprincipal\" class=\"the-form\">
              <div id=\"exito\">
                  <form method=\"post\" action=\"/maleteo\" id=\"formulario\">  
                  <p class=\"the-form__title heading3\">
                   
                    Solicita una demo
                   {{ form_start(DemoForm)}}
                    <div class=\"formulario__form\" id=\"formulario\" >
                    <p class=\"formulario__demo\">
                    {{ form_row(DemoForm.nombre, {label: \"Nombre\", attr:{'class': \"the-form__input\", placeholder: \"Nombre\"}}) }}
                    {{ form_row(DemoForm.email, {label: \"email\", attr:{'class': \"the-form__input\", placeholder: \"Email\"}}) }}
                    {{ form_row(DemoForm.horario, {label: \"horario\", attr:{'class': \"the-form__input\", placeholder: \"Mañana\"}}) }}
                    {{ form_row(DemoForm.ciudad, {label: \"ciudad\", attr:{'class': \"the-form__input the-form__select\", placeholder: \"Tres Cantos\"}}) }}
                    {{ form_row(DemoForm.Politicadeprivacidad, {attr:{'class': \"the-form__group\"}}) }}
                    {{ form_row(DemoForm.Enviar, {attr:{'class': \"the-form__btn btn btn-lg btn-primary\"}}) }}
                    </div>
                    {{ form_end(DemoForm) }}
                    </div>
                    </form>
                  </p>
                  
                  
              </div>
            </div>
          </div>
        </section>

        <section class=\"opinions\">
          <h2 class=\"opinions__title heading2\">
            Opiniones de otros guardianes
          </h2>
          <ul class=\"row\">
            <li class=\"col-md-4\">
              <div class=\"opinion\">
                <blockquote class=\"opinion__blq simple-bubble text-large\">
                  Tras el enorme éxito internacional de su primera colaboración, “Bailar”, que ganó un galardón en los Premios
                </blockquote>
                <p class=\"opinion__author text-large\">
                  Sergio Garnacho
                </p>
                <p class=\"opinion__author-address\">
                  Tetuán, Madrid
                </p>
              </div>
            </li>
            <li class=\"col-md-4\">
              <div class=\"opinion\">
                <blockquote class=\"opinion__blq simple-bubble text-large\">
                  Tras el enorme éxito internacional de su primera colaboración, “Bailar”, que ganó un galardón en los Premios
                </blockquote>
                <p class=\"opinion__author text-large\">
                  Sergio Sánchez
                </p>
                <p class=\"opinion__author-address\">
                  Barrio del Pilar, Madrid
                </p>
              </div>
            </li>
            <li class=\"col-md-4\">
              <div class=\"opinion\">
                <blockquote class=\"opinion__blq simple-bubble text-large\">
                  Tras el enorme éxito internacional de su primera colaboración, “Bailar”, que ganó un galardón en los Premios
                </blockquote>
                <p class=\"opinion__author text-large\">
                  Sergio Garnacho
                </p>
                <p class=\"opinion__author-address\">
                  Tetuán, Madrid
                </p>
              </div>
            </li>
          </ul>
        </section>
      </div>  
    </div>
 <div class=\"mapa \">
    <div class=\"mapa__title\"><h2>Guardianes cerca tuyo</h2>
    </div>
    <div class=\"row\">
        <div class =\"col-xs-4 mapa__valoracion\" id=\"valoracion\">
          
        </div>

        <div class=\" col-xs-8 mapa__map\"><iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d12147.560253557589!2d-3.66576724156495!3d40.43343317544412!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses!2ses!4v1575743718905!5m2!1ses!2ses\" width=\"80%\" height=\"300\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>
        </div>
    </div>
  </div>
</main>
  <footer class=\"footer\">
    <div class=\"wrapper\">
      <div class=\"container-fluid\">
        <img src=\"img/logowhite.svg\"
            alt=\"Maleteo\"
            class=\"footer__logo\">
        <p class=\"footer__claim\">
          Hecho con ❤️en Madrid
        </p>
      </div>
    </div>
  </footer>
</body>

</html>", "maleteo/maleteo.html.twig", "/application/templates/maleteo/maleteo.html.twig");
    }
}
