<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/homeRegistro.html.twig */
class __TwigTemplate_171992b899bc2050217ab9e30021303d0a2454400c54f642ef570199f97b9178 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/homeRegistro.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">

<head>

    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <title>";
        // line 9
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
    ";
        // line 10
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 17
        echo "
</head>

<body>


    <header>
        <div class=\"encabezado\">
            <img src=\"img/hmg.png\">
            <h1>Prueba de código </h1>
            <h2>J. Javier Martínez</h2>
        </div>

    </header>

    <main>
        <div class=\"home\">
            <div class=\"home__iniciar\"><a href=\"";
        // line 34
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("homepage");
        echo "\">Iniciar sesión</a>
               
            </div>
            <div class=\"home__registro\"><a href=\"\">Registrarse</a>
              <form action=\"\" method=\"POST\">
                    <label>Username: </label>
                    <input type=\"text\" name=\"username\" />
                     <label>email: </label>
                    <input type=\"email\" name=\"email\" />
                    <label>Password: </label>
                    <input type=\"password\" name=\"password\" />
                    <input type=\"submit\" name=\"enviar\" value=\"Registrarse\" />
                </form> 
            </div>
        </div>
    </main>
    <footer>
        <div class=\"pie\">
            <p>Prueba realizado el:19/06/2020 </p>
        </div>
    </footer>
</body>

</html>";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 9
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Prueba HMG";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 10
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 11
        echo "    <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css\">
    <link href=\"https://fonts.googleapis.com/css?family=Catamaran:400,500&display=swap\" rel=\"stylesheet\">
    <link rel=\"stylesheet\" href=\"css/estilosHome.css\">
    <script src=\"https://cdn.jsdelivr.net/npm/squirrelly@7.5.0/dist/squirrelly.min.js\"></script>

    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "home/homeRegistro.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  128 => 11,  121 => 10,  108 => 9,  77 => 34,  58 => 17,  56 => 10,  52 => 9,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"en\">

<head>

    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <title>{% block title %}Prueba HMG{% endblock %}</title>
    {%block stylesheets %}
    <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css\">
    <link href=\"https://fonts.googleapis.com/css?family=Catamaran:400,500&display=swap\" rel=\"stylesheet\">
    <link rel=\"stylesheet\" href=\"css/estilosHome.css\">
    <script src=\"https://cdn.jsdelivr.net/npm/squirrelly@7.5.0/dist/squirrelly.min.js\"></script>

    {% endblock %}

</head>

<body>


    <header>
        <div class=\"encabezado\">
            <img src=\"img/hmg.png\">
            <h1>Prueba de código </h1>
            <h2>J. Javier Martínez</h2>
        </div>

    </header>

    <main>
        <div class=\"home\">
            <div class=\"home__iniciar\"><a href=\"{{path('homepage')}}\">Iniciar sesión</a>
               
            </div>
            <div class=\"home__registro\"><a href=\"\">Registrarse</a>
              <form action=\"\" method=\"POST\">
                    <label>Username: </label>
                    <input type=\"text\" name=\"username\" />
                     <label>email: </label>
                    <input type=\"email\" name=\"email\" />
                    <label>Password: </label>
                    <input type=\"password\" name=\"password\" />
                    <input type=\"submit\" name=\"enviar\" value=\"Registrarse\" />
                </form> 
            </div>
        </div>
    </main>
    <footer>
        <div class=\"pie\">
            <p>Prueba realizado el:19/06/2020 </p>
        </div>
    </footer>
</body>

</html>", "home/homeRegistro.html.twig", "/application/templates/home/homeRegistro.html.twig");
    }
}
