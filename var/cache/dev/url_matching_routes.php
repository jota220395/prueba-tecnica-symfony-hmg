<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/' => [[['_route' => 'home', '_controller' => 'App\\Controller\\HomeController::index'], null, null, null, false, false, null]],
        '/insert' => [[['_route' => 'crear_usuarios', '_controller' => 'App\\Controller\\MainController::demoinsert'], null, null, null, false, false, null]],
        '/lista' => [[['_route' => 'listar_usuarios', '_controller' => 'App\\Controller\\MainController::listarUsuarios'], null, null, null, false, false, null]],
        '/usuarios/nuevo' => [[['_route' => 'nuevo_usuario', '_controller' => 'App\\Controller\\MainController::nuevoUsuario'], null, null, null, false, false, null]],
        '/registro' => [[['_route' => 'registro', '_controller' => 'App\\Controller\\RegisterController::register'], null, null, null, false, false, null]],
        '/login' => [[['_route' => 'app_login', '_controller' => 'App\\Controller\\SecurityController::login'], null, null, null, false, false, null]],
        '/buenas/tardes' => [[['_route' => 'index', '_controller' => 'App\\Controller\\DefaultController::index'], null, null, null, false, false, null]],
        '/saludos' => [[['_route' => 'saludos', '_controller' => 'App\\Controller\\DefaultController::saludos'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/_error/(\\d+)(?:\\.([^/]++))?(*:35)'
                .'|/usuarios/e(?'
                    .'|ditar/([^/]++)(*:70)'
                    .'|liminar/([^/]++)(*:93)'
                .')'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        35 => [[['_route' => '_preview_error', '_controller' => 'error_controller::preview', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        70 => [[['_route' => 'editar_usuario', '_controller' => 'App\\Controller\\MainController::editarUsuario'], ['id'], null, null, false, true, null]],
        93 => [
            [['_route' => 'eliminar_usuario', '_controller' => 'App\\Controller\\MainController::eliminarUsuario'], ['id'], null, null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
